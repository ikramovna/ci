from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from .models import Product

class ProductListCreateAPIViewTests(APITestCase):
    def setUp(self):
        self.list_create_url = reverse('product-list-create')

    def test_list_products(self):
        # Create some sample products
        Product.objects.create(name='Product 1', price=10.0)
        Product.objects.create(name='Product 2', price=20.0)

        # Make a GET request to the list endpoint
        response = self.client.get(self.list_create_url)

        # Check that the response status code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check that the response contains the expected number of products
        self.assertEqual(len(response.data), 2)

    def test_create_product(self):
        # Data for creating a new product
        data = {'name': 'New Product', 'price': 15.0}

        # Make a POST request to create a new product
        response = self.client.post(self.list_create_url, data)

        # Check that the response status code is 201 Created
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Check that the product was created in the database
        self.assertTrue(Product.objects.filter(name='New Product').exists())
