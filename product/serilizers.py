from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from product.models import Product


class ProductModelSerializer(ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    class Meta:
        model = Product
        fields = ('id', 'name', 'description', 'price', 'user')
