from django.urls import path

from product.views import ProductListCreateAPIView

urlpatterns = [
    path('product', ProductListCreateAPIView.as_view(), name='product-list-create'),
]
