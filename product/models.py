from django.db.models import *


class Product(Model):
    name = CharField(max_length=255)
    description = TextField()
    price = DecimalField(max_digits=10, decimal_places=2)
    user = ForeignKey('auth.User', on_delete=CASCADE)

    def __str__(self):
        return self.name