# Use a lightweight Python base image
FROM python:3.11-slim

# Set the working directory
WORKDIR /app

# Copy the requirements file
COPY requirements.txt requirements.txt

# Install dependencies, caching for faster rebuilds
RUN pip install --upgrade pip && \
    pip install -r requirements.txt --no-cache-dir

# Copy your application code
COPY . .

# Expose the port for your application (if applicable)
EXPOSE 8000

# Set the default command for running the container
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
