mig:
	./manage.py makemigrations
	./manage.py migrate
admin:
	python3 manage.py createsuperuser  --username admin --email  admin@mail.com

req:
	pip3 freeze > requirements.txt

install-req:
	pip3 install -r requirements.txt

run:
	python3 manage.py runserver

run_build:
	docker build -t python-app .

test:
	python3 manage.py test

pull:
	git pull

gunicorn:
	systemctl restart gunicorn.socket gunicorn.service

venv:
	source venv/bin/activate


#docker run -d -p 8000:8000 python-app


